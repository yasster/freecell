import Vue from "vue";
import Router from "vue-router";

import App from "./App.vue";
import Menu from "./views/Menu.vue";
import Freecell from "./views/Freecell.vue";

// Setting up Vue with the Vue Router...
Vue.use(Router);

let router = new Router({
    mode:'history',
    routes: [{
        path: "/",
        component:App,
        children: [
            {
                path: "/",
                name: "Menu",
                component: Menu
            },
            {
                path: "/freecell",
                name: "Freecell",
                component: Freecell
            }   
        ]},
        {
            path: "*",
            redirect: "/",
        }
    ]
});

export default router;
